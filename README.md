#WHHoloColorPicker

This is a complete rewrite of Marie Schweiz' <http://marie-schweiz.de/> design for the Holo ColorPicker.

Obviously this implementation will look like other implementations based on Ms Schweiz's design. However, the code is completely original and adds more functionality then available elsewhere.

The color picker is ready to use as is. But you can also extend it with more functionality if you wish by creating your own bars.

![Alt text](./screenshots/Screenshot_vertical.png "Vertical")


![Alt text](./screenshots/Screenshot_horizontal.png "Horizontal")

## Features
  * Dynamic bars update to show their current state
  * Bars can be horizontal or vertical
  * Bars can be LTR or RTL
  * Supports RGB and/or HSV

## Documentation
 The complete color picker is composed of modules which work together to create a color that the user can choose. It comes with a central ColorPicker, a Hue wheel and 7 color bars.
### ColorPicker
 The color picker represents the previously selected color as well as the newly configured color. Clicking on the previously selected color will reset the color picker to that value. 
 
### Hue wheel
 The hue wheel will normally be positioned around the ColorPicker
 
### Color bars
 I've implemented 7 color bars that can be used with the ColorPicker. It's up to you to decide which ones you want to use in your application.
 Usually you would choose one of these configurations :
 
  * Red, Green and Blue bars
  * Red, Green and Blue bars and Opacity bar
  * SV, Saturation and Value bars and Hue wheel
  * SV, Saturation and Value bars, Hue wheel, and Opacity bar

###Dependency
Adding it as a dependency to your project.

	dependencies {
    	compile 'com.wallacehub:WHHoloColorPicker:1.0'
	}

### Layout
The best way to add the color picker to your application is to use a ConstraintLayout. You can 
see an example on how to accomplish this in the sample app.

### Java
To connect the bars with the colorpicker and to get the selected color.

```java
final ColorPicker picker = (ColorPicker) findViewById(R.id.picker);

// Add all of the bars that you want to use. 
picker.addBar((Bar_Base) findViewById(R.id.svbar));
picker.addBar((Bar_Base) findViewById(R.id.opacitybar));
picker.addBar((Bar_Base) findViewById(R.id.saturationbar));

// Set the old color
picker.setOldColor(Color.GREEN);

// You can also set the new color dynamically
picker.setNewColor(Color.BLUE);

// Get the newly configured color when the user is finished configuring it.
picker.getNewColor();

// adds listener to the colorpicker which is implemented in the activity
picker.setOnColorChangedListener(this);
```

## License
	
MIT License

Copyright (c) 2017 Michael Wallace, Wallace Hub Software 

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE. 	

