package com.wallacehub.whholocolorpicker.whholocolorpicker;

import android.content.Context;
import android.graphics.Color;
import android.graphics.LinearGradient;
import android.graphics.Shader;
import android.util.AttributeSet;
import com.wallacehub.utils.logging.WHLog;

public class ValueBar extends Bar_Base
{
	//<editor-fold desc="TAG">
	@SuppressWarnings({"FieldNameHidesFieldInSuperclass", "unused"})
	public static final String TAG = ValueBar.class.getSimpleName();

	static {
		WHLog.LOG_LEVELS.put(TAG, WHLog.WARN);
	}
	//</editor-fold>

	//<editor-fold desc="Variables">
	/**
	 * Factor used to calculate the position of the pointer on the bar.
	 */
	protected float mPosToValueFactor;

	/**
	 * Factor used to calculate the value vis-a-vis the pointer on the bar.
	 */
	protected float mValueToPosFactor;
	//</editor-fold>


	//<editor-fold desc="Constructors">
	public ValueBar(Context context) {
		this(context, null);
	}


	public ValueBar(Context context, AttributeSet attrs) {
		this(context, attrs, 0);
	}


	public ValueBar(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
	}


	@SuppressWarnings("UnusedParameters")
	protected void init(final Context context, final AttributeSet attrs, final int defStyle) {
		super.init(context, attrs, defStyle);

		mPosToValueFactor = 1 / m_barLength;
		mValueToPosFactor = m_barLength / 1;
	}
	//</editor-fold>


	@Override
	public void updateColor(final int alpha, final float[] hsvColor) {
		super.updateColor(alpha, hsvColor);

		if (0 == hsvColor[1]) {
			hsvColor[2] = 1;
		}

		m_pointerPaint.setColor(Color.HSVToColor(m_color));

		updateShader();

		updatePointerPosition();

		invalidate();
	}


	@Override
	protected void updateShader() {
		WHLog.d(TAG, "updateShader");

		final float width = m_orientationHorizontal == ORIENTATION_HORIZONTAL ? m_barLength : m_barThickness;
		final float height = m_orientationHorizontal == ORIENTATION_HORIZONTAL ? m_barThickness : m_barLength;

		final float[] color = new float[3];
		color[0] = m_color[0];
		color[1] = 1;
		color[2] = 1;

		m_barShader = new LinearGradient(0, 0, width, height, new int[]{Color.HSVToColor(0xFF, color), Color.BLACK}, null, Shader.TileMode.CLAMP);

		mBarPaint.setShader(m_barShader);
	}


	@Override
	protected void updatePosToValFactor() {
		mPosToValueFactor = 1 / m_barLength;
	}


	@Override
	protected void updateValToPosFactor() {
		mValueToPosFactor = m_barLength / 1;
	}


	@Override
	protected void updatePointerPosition() {
		if (m_orientationHorizontal) {
			float newPosition = m_barLength - (mValueToPosFactor * m_color[2]) + m_pointerHaloWidth;
			m_pointerPosition = clampf(newPosition, mBarRect.left, mBarRect.right);
		}
		else {
			float newPosition = m_barLength - (mValueToPosFactor * m_color[2]) + m_pointerHaloWidth;
			m_pointerPosition = clampf(newPosition, mBarRect.top, mBarRect.bottom);
		}
	}


	@Override
	protected void updatePointerPosition(final float _x, final float _y) {
		float newPosition = m_orientationHorizontal == ORIENTATION_HORIZONTAL ? _x : _y;

		if (m_orientationHorizontal) {
			m_pointerPosition = clampf(newPosition, mBarRect.left, mBarRect.right);
		}
		else {
			m_pointerPosition = clampf(newPosition, mBarRect.top, mBarRect.bottom);
		}
	}


	/**
	 * Calculate the color selected by the pointer on the bar.
	 */
	@Override
	protected void updatePointerColor() {
		WHLog.d(TAG, "updatePointerColor");

		float coordinate = m_pointerPosition - mBarRect.left;

		m_color[2] = clampf(1 - mPosToValueFactor * coordinate, 0, 1);

		m_pointerPaint.setColor(Color.HSVToColor(m_color));
	}
}