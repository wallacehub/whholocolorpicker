package com.wallacehub.whholocolorpicker.whholocolorpicker;

import android.content.Context;
import android.content.res.Resources;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.SweepGradient;
import android.util.AttributeSet;
import android.view.MotionEvent;
import com.wallacehub.whholocolorpicker.R;
import com.wallacehub.utils.logging.WHLog;


/**
 * @author :  Mike Wallace (+MikeWallaceDev) <mike@wallacehub.com> on 11/21/16.
 */
public class HueWheel extends Bar_Base
{
	//<editor-fold desc="TAG" defaultstate="collapsed">
	@SuppressWarnings({"FieldNameHidesFieldInSuperclass", "unused"})
	public static final String TAG = HueWheel.class.getSimpleName();

	static {
		WHLog.LOG_LEVELS.put(TAG, WHLog.WARN);
	}
	//</editor-fold>


	//<editor-fold defaultstate="collapsed" desc="Variables">
	/**
	 * Colors to construct the color wheel using {@link android.graphics.SweepGradient}.
	 */
	private static final int[] COLORS = new int[]{0xFFFF0000, 0xFFFF00FF, 0xFF0000FF, 0xFF00FFFF, 0xFF00FF00, 0xFFFFFF00, 0xFFFF0000};

	/**
	 * The pointer's position expressed as angle (in rad).
	 */
	private float[] m_pointerPosition = angleToPosition(0);

	/**
	 * The radius of the color wheel.
	 */
	private float m_colorWheelRadius;
	private float m_preferredColorWheelRadius;

	/**
	 * {@code TouchAnywhereOnColorWheelEnabled} instance used to control if the color wheel accepts input anywhere on the wheel or just
	 * on the halo.
	 */
	private boolean mTouchAnywhereOnColorWheelEnabled = true;

	/**
	 * Number of pixels the origin of this view is moved in X- and Y-direction.
	 * <p>
	 * <p>
	 * We use the center of this (quadratic) View as origin of our internal
	 * coordinate system. Android uses the upper left corner as origin for the
	 * View-specific coordinate system. So this is the m_alpha we use to translate
	 * from one coordinate system to the other.
	 * </p>
	 * <p>
	 * <p>
	 * Note: (Re)calculated in {@link #onMeasure(int, int)}.
	 * </p>
	 *
	 * @see #onDraw(android.graphics.Canvas)
	 */
	private float m_translationOffsetX;
	private float m_translationOffsetY;
	//</editor-fold>


	//<editor-fold desc="Constructors" defaultstate="collapsed">
	public HueWheel(Context context) {
		this(context, null);
	}


	public HueWheel(Context context, AttributeSet attrs) {
		this(context, attrs, 0);
	}


	public HueWheel(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
	}


	@SuppressWarnings("UnusedParameters")
	protected void init(final Context context, final AttributeSet attrs, final int defStyle) {
		super.init(context, attrs, defStyle);

		final TypedArray typedArray = context.obtainStyledAttributes(attrs, R.styleable.ColorWheel, defStyle, 0);
		final Resources resources = context.getResources();

		m_colorWheelRadius = typedArray.getDimensionPixelSize(R.styleable.ColorWheel_wheel_radius, resources.getDimensionPixelSize(R.dimen.color_wheel_radius));
		m_preferredColorWheelRadius = m_colorWheelRadius;

		typedArray.recycle();

		m_color = new float[]{100, 0.5f, 0.5f};

		mBarPaint.setShader(new SweepGradient(0, 0, COLORS, null));
		mBarPaint.setStyle(Paint.Style.STROKE);
		mBarPaint.setStrokeWidth(m_barThickness);

		m_pointerPaint.setStyle(Paint.Style.FILL);
	}
	//</editor-fold>


	/**
	 * Get the base color, which is the color pointed to by the spinner
	 *
	 * @return The RGB m_alpha of the new color
	 */
	public int getHue() {
		return (int) m_color[0];
	}


	public void setTouchAnywhereOnColorWheelEnabled(boolean TouchAnywhereOnColorWheelEnabled) {
		mTouchAnywhereOnColorWheelEnabled = TouchAnywhereOnColorWheelEnabled;
	}


	@Override
	protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
		final float intrinsicSize = 2 * (m_preferredColorWheelRadius + m_pointerHaloWidth);

		int widthMode = MeasureSpec.getMode(widthMeasureSpec);
		int widthSize = MeasureSpec.getSize(widthMeasureSpec);
		int heightMode = MeasureSpec.getMode(heightMeasureSpec);
		int heightSize = MeasureSpec.getSize(heightMeasureSpec);

		float width;
		float height;

		if (widthMode == MeasureSpec.EXACTLY) {
			width = widthSize;
		}
		else if (widthMode == MeasureSpec.AT_MOST) {
			width = Math.min(intrinsicSize, widthSize);
		}
		else {
			width = intrinsicSize;
		}

		if (heightMode == MeasureSpec.EXACTLY) {
			height = heightSize;
		}
		else if (heightMode == MeasureSpec.AT_MOST) {
			height = Math.min(intrinsicSize, heightSize);
		}
		else {
			height = intrinsicSize;
		}

		int min = Math.round(Math.min(width, height));
		setMeasuredDimension(min, min);
	}


	@Override
	protected void onSizeChanged(int width, int height, int oldW, int oldH) {
		int min = Math.min(width, height);

		m_translationOffsetX = width / 2f;
		m_translationOffsetY = height / 2f;

		// fill the rectangle instances.
		m_colorWheelRadius = min / 2 - m_barThickness - m_pointerRadius;
		mBarRect.set(-m_colorWheelRadius, -m_colorWheelRadius, m_colorWheelRadius, m_colorWheelRadius);

		updateShader();
		updatePointerPosition();
	}


	@Override
	protected void onDraw(Canvas canvas) {
		// All of our positions are using our internal coordinate system. Instead of translating
		// them we let Canvas do the work for us.
		canvas.translate(m_translationOffsetX, m_translationOffsetY);

		// Draw the color wheel.
		canvas.drawOval(mBarRect, mBarPaint);

		// Draw the pointer's "halo"
		canvas.drawCircle(m_pointerPosition[0], m_pointerPosition[1], m_pointerRadius - m_pointerHaloWidth / 2, m_pointerHaloPaint);

		// Draw the pointer (the currently selected color) slightly smaller on top.
		canvas.drawCircle(m_pointerPosition[0], m_pointerPosition[1], m_pointerRadius - m_pointerHaloWidth, m_pointerPaint);
	}


	@Override
	public boolean onTouchEvent(MotionEvent event) {
		WHLog.d(TAG, "onTouchEvent");

		final float x = event.getX() - m_translationOffsetX;
		final float y = event.getY() - m_translationOffsetY;
		WHLog.d(TAG, "x = [ " + x + " ] \ny = [ " + y + " ] \n");

		switch (event.getAction()) {
			case MotionEvent.ACTION_DOWN: {
				// Check whether the user pressed on the pointer.

				if (!clickedPointer(x, y) && !(mTouchAnywhereOnColorWheelEnabled && clickedOnWheel(x, y))) {
					return false;
				}

				m_IsPointerMoving = true;

				updatePointerPosition(x, y);

				updatePointerColor();

				invalidate();
			}
			break;

			case MotionEvent.ACTION_MOVE: {
				if (!m_IsPointerMoving) {
					return false;
				}

				updatePointerPosition(x, y);

				updatePointerColor();

				invalidate();
			}
			break;

			case MotionEvent.ACTION_UP: {
				if (!m_IsPointerMoving) {
					return false;
				}
				m_IsPointerMoving = false;

				updatePointerPosition(x, y);

				updatePointerColor();

				invalidate();
			}
			break;
		}

		if (null != m_onColorChangedListener) {
			m_onColorChangedListener.setColor(m_alpha, m_color);
		}

		return true;
	}


	private boolean clickedPointer(final float x, final float y) {
		return x >= (m_pointerPosition[0] - m_pointerHaloWidth) && x <= (m_pointerPosition[0] + m_pointerHaloWidth)
				&& y >= (m_pointerPosition[1] - m_pointerHaloWidth) && y <= (m_pointerPosition[1] + m_pointerHaloWidth);
	}


	private boolean clickedOnWheel(final float x, final float y) {
		return x * x + y * y <= (m_colorWheelRadius + m_pointerRadius) * (m_colorWheelRadius + m_pointerRadius);
	}


	/**
	 * Calculate the pointer's coordinates on the color wheel using the supplied
	 * angle.
	 *
	 * @param angle The position of the pointer expressed as angle (in rad).
	 * @return The coordinates of the pointer's center in our internal
	 * coordinate system.
	 */
	private float[] angleToPosition(float angle) {
		WHLog.d(TAG, "angleToPosition");
		WHLog.d(TAG, "angle = [ " + angle + " ] \n");

		float x = (float) (m_colorWheelRadius * Math.cos(angle));
		float y = (float) (m_colorWheelRadius * Math.sin(angle));

		WHLog.d(TAG, "x = [ " + x + " ] \n");
		WHLog.d(TAG, "y = [ " + y + " ] \n");

		return new float[]{x, y};
	}


	/**
	 * Called by the color picker
	 */
	@Override
	public void updateColor(final int alpha, final float[] hsvColor) {
		super.updateColor(alpha, hsvColor);

		//TODO maybe make this an option?
//		m_pointerPaint.setColor(Color.HSVToColor(m_alpha, m_color));

		m_pointerPaint.setColor(Color.HSVToColor(m_color));

		updatePointerPosition();

		invalidate();
	}


	@Override
	protected void updateShader() {
		mBarPaint.setStrokeWidth(m_barThickness);
	}


	@Override
	protected void updatePosToValFactor() { }


	@Override
	protected void updateValToPosFactor() { }


	@Override
	protected void updatePointerPosition() {
		WHLog.d(TAG, "updatePointerPosition");
		WHLog.d(TAG, "m_hue = [ " + m_color[0] + " ] \n");

		float rad = (float) Math.toRadians(m_color[0]);

		if (rad > Math.PI) {
			rad -= 2 * Math.PI;
		}

		float pointX = (float) (m_colorWheelRadius * Math.cos(rad));
		float pointY = (float) (m_colorWheelRadius * Math.sin(rad));

		WHLog.d(TAG, "pointX = [ " + pointX + " ] \n");
		WHLog.d(TAG, "pointY = [ " + pointY + " ] \n");

		m_pointerPosition[0] = pointX;
		m_pointerPosition[1] = -pointY;
	}


	@Override
	protected void updatePointerPosition(final float _x, final float _y) {
		WHLog.d(TAG, "updatePointerPosition");
		WHLog.d(TAG, "_x = [ " + _x + " ] \n_y = [ " + _y + " ] \n");

		final double rad = Math.atan2(_y, _x);

		final float pointX = (float) (m_colorWheelRadius * Math.cos(rad));
		final float pointY = (float) (m_colorWheelRadius * Math.sin(rad));

		WHLog.d(TAG, "pointX = [ " + pointX + " ] \n");
		WHLog.d(TAG, "pointY = [ " + pointY + " ] \n");

		m_pointerPosition[0] = pointX;
		m_pointerPosition[1] = pointY;
	}


	/**
	 * Calculate the color selected by the pointer on the bar.
	 */
	@Override
	protected void updatePointerColor() {
		WHLog.d(TAG, "updatePointerColor");

		double angle = Math.toDegrees(Math.atan2(m_pointerPosition[0], m_pointerPosition[1])) - 90;
		if (angle < 0) {
			angle += 360;
		}

		m_color[0] = (float) angle;

		WHLog.d(TAG, "m_color[0] = " + m_color[0]);
		m_pointerPaint.setColor(Color.HSVToColor(m_color));
	}
}
