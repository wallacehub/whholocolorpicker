package com.wallacehub.whholocolorpicker.whholocolorpicker;

import android.content.Context;
import android.graphics.Color;
import android.graphics.LinearGradient;
import android.graphics.Shader;
import android.util.AttributeSet;
import com.wallacehub.utils.logging.WHLog;


public class SVBar extends Bar_Base
{
	//<editor-fold desc="TAG">
	@SuppressWarnings({"FieldNameHidesFieldInSuperclass", "unused"})
	public static final String TAG = SVBar.class.getSimpleName();

	static {
		WHLog.LOG_LEVELS.put(TAG, WHLog.WARN);
	}
	//</editor-fold>


	//<editor-fold desc="Variables">
	/**
	 * Factor used to calculate the position of the pointer on the bar.
	 */
	private float m_posToValueFactor;

	/**
	 * Factor used to calculate the value vis-a-vis the pointer on the bar.
	 */
	private float m_valueToPosFactor;
	//</editor-fold>


	//<editor-fold desc="Constructors">
	public SVBar(Context context) {
		this(context, null);
	}


	public SVBar(Context context, AttributeSet attrs) {
		this(context, attrs, 0);
	}


	public SVBar(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
	}


	@SuppressWarnings("UnusedParameters")
	protected void init(final Context context, final AttributeSet attrs, final int defStyle) {
		super.init(context, attrs, defStyle);

		m_posToValueFactor = 1 / (m_barLength / 2);
		m_valueToPosFactor = (m_barLength / 2) / 1;
	}
	//</editor-fold>


	@Override
	public void updateColor(final int alpha, final float[] hsvColor) {
		super.updateColor(alpha, hsvColor);

		m_pointerPaint.setColor(Color.HSVToColor(m_color));

		updateShader();

		updatePointerPosition();

		invalidate();
	}


	@Override
	protected void updateShader() {
		WHLog.d(TAG, "updateShader");

		final float width = m_orientationHorizontal == ORIENTATION_HORIZONTAL ? m_barLength : m_barThickness;
		final float height = m_orientationHorizontal == ORIENTATION_HORIZONTAL ? m_barThickness : m_barLength;

		final float[] color = new float[3];
		color[0] = m_color[0];
		color[1] = 1;
		color[2] = 1;

		m_barShader = new LinearGradient(0, 0, width, height, new int[]{0xffffffff, Color.HSVToColor(color), 0xff000000}, null, Shader.TileMode.CLAMP);

		mBarPaint.setShader(m_barShader);
	}


	@Override
	protected void updatePosToValFactor() {
		m_posToValueFactor = 1 / (m_barLength / 2);
	}


	@Override
	protected void updateValToPosFactor() {
		m_valueToPosFactor = (m_barLength / 2) / 1;
	}


	@Override
	protected void updatePointerPosition() {
		if (m_orientationHorizontal) {
			float newPosition;
			if (m_color[1] < m_color[2]) {
				newPosition = m_valueToPosFactor * m_color[1] + mBarRect.left;
			}
			else {
				newPosition = m_valueToPosFactor * (1 - m_color[2]) + mBarRect.left + (m_barLength / 2);
			}

			m_pointerPosition = clampf(newPosition, mBarRect.left, mBarRect.right);
		}
		else {
			float newPosition;
			if (m_color[1] < m_color[2]) {
				newPosition = m_valueToPosFactor * m_color[1] + mBarRect.top;
			}
			else {
				newPosition = m_valueToPosFactor * (1 - m_color[2]) + mBarRect.top + (m_barLength / 2);
			}

			m_pointerPosition = clampf(newPosition, mBarRect.top, mBarRect.bottom);
		}
	}


	@Override
	protected void updatePointerPosition(final float _x, final float _y) {
		if (m_orientationHorizontal) {
			m_pointerPosition = clampf(_x, mBarRect.left, mBarRect.right);
		}
		else {
			m_pointerPosition = clampf(_y, mBarRect.top, mBarRect.bottom);
		}
	}


	/**
	 * Calculate the color selected by the pointer on the bar.
	 */
	@Override
	protected void updatePointerColor() {

		float coordinate = m_pointerPosition - mBarRect.left;

		if (coordinate <= m_barLength / 2) {
			m_color[1] = m_posToValueFactor * coordinate;
			m_color[2] = 1;
		}
		else if (coordinate > m_barLength / 2) {
			m_color[1] = 1;
			m_color[2] = 1 - m_posToValueFactor * (coordinate - m_barLength / 2);
		}

		m_pointerPaint.setColor(Color.HSVToColor(m_color));
	}
}