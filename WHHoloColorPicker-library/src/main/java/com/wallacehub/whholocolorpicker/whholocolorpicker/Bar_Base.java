package com.wallacehub.whholocolorpicker.whholocolorpicker;

import android.content.Context;
import android.content.res.Resources;
import android.content.res.TypedArray;
import android.graphics.*;
import android.os.Bundle;
import android.os.Parcelable;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;
import com.wallacehub.whholocolorpicker.R;
import com.wallacehub.utils.logging.WHLog;

/**
 * @author :  Mike Wallace (+MikeWallaceDev) <mike@wallacehub.com> on 11/17/16.
 */
public abstract class Bar_Base extends View
{
	//<editor-fold defaultstate="collapsed" desc="TAG">
	@SuppressWarnings({"FieldNameHidesFieldInSuperclass", "unused"})
	public static final String TAG = Bar_Base.class.getSimpleName();

	static {
		WHLog.LOG_LEVELS.put(TAG, WHLog.WARN);
	}
	//</editor-fold>

	//<editor-fold desc="Constants">
	/**
	 * This is not a setting! Do not modify this.
	 * Constants used to identify orientation.
	 */
	protected static final boolean ORIENTATION_HORIZONTAL = true;

	/**
	 * This is not a setting! Do not modify this.
	 * Direction of the bar.
	 */
	protected static final boolean DIRECTION_REVERSE = false; // Left to Right, or Top to Bottom for vertical
	//</editor-fold>


	//<editor-fold desc="Interfaces">


	/**
	 * This is called as the new color is changed, or if the base color is changed by this bar.
	 * Typically the listener is the ColorPicker widget
	 */
	public interface OnColorChangedListener
	{
		void setColor(final int alpha, final float[] newColor);
	}
	//</editor-fold>


	//<editor-fold desc="Variables" defaultstate="collapsed">
	/**
	 * Constants used to save/restore the instance state.
	 */
	protected static final String STATE_PARENT_STATE = "STATE_PARENT_STATE";
	protected static final String STATE_NEW_COLOR    = "STATE_NEW_COLOR";
	protected static final String STATE_ORIENTATION  = "STATE_ORIENTATION";


	/**
	 * Default orientation of the bar.
	 */
	protected static final boolean ORIENTATION_DEFAULT = ORIENTATION_HORIZONTAL;


	/**
	 * Default orientation of the bar.
	 */
	protected static final boolean DIRECTION_DEFAULT = DIRECTION_REVERSE;


	/**
	 * The ARGB of the currently selected color
	 */
	protected float[] m_color = new float[3];
	protected int m_alpha;

	/**
	 * The thickness of the bar.
	 */
	protected int m_barThickness;


	/**
	 * The requested length of the bar
	 */
	protected float m_preferredBarLength;

	/**
	 * The actual length of the bar. This does not include any chrome, such as the pointer which overflows over the bar
	 */
	protected float m_barLength;

	/**
	 * The radius of the pointer.  This includes the halo drawn around the pointer
	 */
	protected float m_pointerRadius;

	/**
	 * The width of the halo of the pointer.
	 */
	protected int m_pointerHaloWidth;

	/**
	 * Used to toggle orientation between vertical and horizontal.
	 */
	protected boolean m_orientationHorizontal;

	/**
	 * Used to toggle the direction of the bars from RTL to LTR  or TTB BTT
	 */
	protected boolean m_directionReversed;

	/**
	 * The rectangle enclosing the bar.  This is used to draw the bar with the paint
	 */
	protected RectF mBarRect = new RectF();

	/**
	 * The position of the pointer on the bar.
	 */
	protected float m_pointerPosition;

	/**
	 * {@code Paint} instance used to draw the bar.
	 */
	protected Paint mBarPaint;

	/**
	 * {@code Paint} instance used to draw the pointer.
	 */
	protected Paint m_pointerPaint;

	/**
	 * {@code Paint} instance used to draw the halo of the pointer.
	 */
	protected Paint m_pointerHaloPaint;

	/**
	 * {@code Shader} instance used to fill the m_barShader of the paint.
	 */
	protected Shader m_barShader;

	/**
	 * {@code true} if the user clicked on the pointer to start the move mode. <br>
	 * {@code false} once the user stops touching the screen.
	 *
	 * @see #onTouchEvent(android.view.MotionEvent)
	 */
	protected boolean m_IsPointerMoving;

	protected OnColorChangedListener m_onColorChangedListener;
	//</editor-fold>


	//<editor-fold desc="constructors"  defaultstate="collapsed">
	public Bar_Base(Context context) {
		this(context, null);
	}


	public Bar_Base(Context context, AttributeSet attrs) {
		this(context, attrs, 0);
	}


	public Bar_Base(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);

		init(context, attrs, defStyle);
	}


	protected void init(final Context context, final AttributeSet attrs, final int defStyle) {
		final TypedArray typedArray = context.obtainStyledAttributes(attrs, R.styleable.ColorBars, defStyle, 0);
		final Resources resources = context.getResources();

		m_barThickness = typedArray.getDimensionPixelSize(R.styleable.ColorBars_thickness, resources.getDimensionPixelSize(R.dimen.bar_thickness));
		m_preferredBarLength = m_barLength = typedArray.getDimensionPixelSize(R.styleable.ColorBars_length, resources.getDimensionPixelSize(R.dimen.bar_length));

		m_pointerRadius = typedArray.getDimension(R.styleable.ColorBars_pointer_radius, resources.getDimension(R.dimen.bar_pointer_radius));

		m_pointerHaloWidth = typedArray.getDimensionPixelSize(R.styleable.ColorBars_pointer_halo_width, resources.getDimensionPixelSize(R.dimen.bar_pointer_halo_width));
		m_orientationHorizontal = typedArray.getBoolean(R.styleable.ColorBars_orientation_horizontal, ORIENTATION_DEFAULT);

		m_directionReversed = typedArray.getBoolean(R.styleable.ColorBars_reverse, DIRECTION_DEFAULT);
		typedArray.recycle();

		m_color = new float[]{0, 0.5f, 0.5f};

		mBarPaint = new Paint(Paint.ANTI_ALIAS_FLAG);
		mBarPaint.setShader(m_barShader);
		mBarPaint.setStyle(Paint.Style.FILL);
		mBarPaint.setStrokeWidth(m_barThickness);

		m_pointerPosition = m_pointerHaloWidth;

		m_pointerHaloPaint = new Paint(Paint.ANTI_ALIAS_FLAG);
		m_pointerHaloPaint.setColor(Color.BLACK);
		m_pointerHaloPaint.setAlpha(0x50);
		m_pointerHaloPaint.setStyle(Paint.Style.STROKE);
		m_pointerHaloPaint.setStrokeWidth(m_pointerHaloWidth);

		m_pointerPaint = new Paint(Paint.ANTI_ALIAS_FLAG);
		m_pointerPaint.setColor(Color.HSVToColor(m_alpha, m_color));

		m_onColorChangedListener = null;
	}
	//</editor-fold>


	public void setOnColorChangedListener(final OnColorChangedListener listener) {
		m_onColorChangedListener = listener;
	}


	@Override
	protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
		final float pointerRadiusTwice = m_pointerRadius * 2;

		final float intrinsicLength = m_preferredBarLength + pointerRadiusTwice;

		// Variable orientation
		int measureSpec;
		if (m_orientationHorizontal == ORIENTATION_HORIZONTAL) {
			measureSpec = widthMeasureSpec;
		}
		else {
			measureSpec = heightMeasureSpec;
		}

		final int lengthSize = MeasureSpec.getSize(measureSpec);

		switch (MeasureSpec.getMode(measureSpec)) {
			case MeasureSpec.EXACTLY: {
				m_barLength = lengthSize - pointerRadiusTwice;
			}
			break;

			case MeasureSpec.AT_MOST: {
				m_barLength = Math.min(intrinsicLength, lengthSize) - pointerRadiusTwice;
			}
			break;

			case MeasureSpec.UNSPECIFIED:
			default: {
				m_barLength = intrinsicLength - pointerRadiusTwice;
			}
			break;
		}

		if (m_orientationHorizontal == ORIENTATION_HORIZONTAL) {
			setMeasuredDimension(Math.round(m_barLength + pointerRadiusTwice), Math.round(pointerRadiusTwice));
		}
		else {
			setMeasuredDimension(Math.round(pointerRadiusTwice), Math.round(m_barLength + pointerRadiusTwice));
		}
	}


	@Override
	protected void onSizeChanged(int width, int height, int oldW, int oldH) {
		super.onSizeChanged(width, height, oldW, oldH);

		if (m_orientationHorizontal == ORIENTATION_HORIZONTAL) {
			m_barLength = width - (m_pointerRadius * 2);

			mBarRect.set(m_pointerRadius, m_pointerRadius - (m_barThickness / 2),
							 m_pointerRadius + m_barLength, +m_pointerRadius + (m_barThickness / 2));
		}
		else {
			m_barLength = height - (m_pointerRadius * 2);

			mBarRect.set(m_pointerRadius - (m_barThickness / 2), m_pointerRadius,
							 m_pointerRadius + (m_barThickness / 2), m_barLength + m_pointerRadius);
		}

		updateShader();
		updatePosToValFactor();
		updateValToPosFactor();
		updatePointerPosition();
	}


	@Override
	protected void onDraw(Canvas canvas) {
		// Draw the bar.
		canvas.drawRect(mBarRect, mBarPaint);

		// Calculate the center of the pointer.
		float cX, cY;
		if (m_orientationHorizontal == ORIENTATION_HORIZONTAL) {
			cX = m_pointerPosition;
			cY = m_pointerRadius;
		}
		else {
			cX = m_pointerRadius;
			cY = m_pointerPosition;
		}

		// Draw the pointer halo.
		canvas.drawCircle(cX, cY, m_pointerRadius - m_pointerHaloWidth / 2, m_pointerHaloPaint);

		// Draw the pointer.
		canvas.drawCircle(cX, cY, m_pointerRadius - m_pointerHaloWidth, m_pointerPaint);
	}


	@Override
	public boolean onTouchEvent(MotionEvent event) {
		getParent().requestDisallowInterceptTouchEvent(true);

		switch (event.getAction()) {
			case MotionEvent.ACTION_DOWN: {
				WHLog.v(TAG, "Clicked POINTER");

				m_IsPointerMoving = true;

				updatePointerPosition(event.getX(), event.getY());

				updatePointerColor();

				invalidate();
			}
			break;

			case MotionEvent.ACTION_MOVE: {
				if (m_IsPointerMoving) {
					// Move the the pointer on the bar.
					updatePointerPosition(event.getX(), event.getY());

					updatePointerColor();

					invalidate();
				}
			}
			break;

			case MotionEvent.ACTION_UP: {
				m_IsPointerMoving = false;

				updatePointerPosition(event.getX(), event.getY());

				updatePointerColor();

				invalidate();
			}
			break;
		}

		if (null != m_onColorChangedListener) {
			m_onColorChangedListener.setColor(m_alpha, m_color);
		}

		return true;
	}


	/**
	 * Get the base color.
	 *
	 * @return The ARGB m_alpha of the base color.
	 */
	public int getColor() {
		return Color.HSVToColor(m_alpha, m_color);
	}


	/**
	 * Get the base color.
	 *
	 * @return The ARGB m_alpha of the base color.
	 */
	public float[] getColorHSV() {
		return m_color;
	}


	@Override
	protected Parcelable onSaveInstanceState() {
		final Bundle state = new Bundle();

		state.putParcelable(STATE_PARENT_STATE, super.onSaveInstanceState());
		state.putInt(STATE_NEW_COLOR, Color.HSVToColor(m_alpha, m_color));
		state.putBoolean(STATE_ORIENTATION, m_orientationHorizontal);

		return state;
	}


	@Override
	protected void onRestoreInstanceState(Parcelable state) {
		if (state instanceof Bundle) { // implicit null check
			final Bundle bundle = (Bundle) state;

			if (bundle.containsKey(STATE_ORIENTATION)) {
				m_orientationHorizontal = bundle.getBoolean(STATE_ORIENTATION);
			}

			if (bundle.containsKey(STATE_NEW_COLOR)) {
				int color = bundle.getInt(STATE_NEW_COLOR);
				float[] hsvColor = new float[3];
				Color.colorToHSV(color, hsvColor);
				updateColor(Color.alpha(color), hsvColor);
			}

			if (bundle.containsKey(STATE_PARENT_STATE)) {
				state = bundle.getParcelable(STATE_PARENT_STATE);
			}
		}

		super.onRestoreInstanceState(state);
	}


	protected static int clamp(int value, int min, int max) {
		if (value < min) {
			return min;
		}
		if (value > max) {
			return max;
		}
		return value;
	}


	protected static float clampf(float value, float min, float max) {
		if (value < min) {
			return min;
		}
		if (value > max) {
			return max;
		}
		return value;
	}


	/**
	 * Request to update the current colors of the bar, as well as update the pointer in the bar
	 *
	 * @param hsvColor The current new color in the picker
	 */
	public void updateColor(int alpha, final float[] hsvColor) {
		WHLog.d(TAG, "updateColor");
		WHLog.d(TAG, "color = [ " + String.format("#%08X", Color.HSVToColor(m_alpha, m_color)) + " ] \n");

		if (alpha == m_alpha
				&& hsvColor[0] == m_color[0]
				&& hsvColor[1] == m_color[1]
				&& hsvColor[2] == m_color[2]) {
			return;
		}

		m_color[0] = hsvColor[0];
		m_color[1] = hsvColor[1];
		m_color[2] = hsvColor[2];

		m_alpha = alpha;
	}


	/**
	 * Called when the pointer color should be updated based on the current pointer position.
	 * This only should update member variables. There is no drawing here.
	 */
	protected abstract void updatePointerColor();

	protected abstract void updateShader();

	protected abstract void updatePosToValFactor();

	protected abstract void updateValToPosFactor();

	/**
	 * Called when the pointer should be repositioned based on the user's touch.
	 * This only should update member variables. There is no drawing here.
	 */
	protected abstract void updatePointerPosition(float x, float y);

	/**
	 * Called when the pointer should be repositioned based on the the current value.
	 * This only should update member variables. There is no drawing here.
	 */
	protected abstract void updatePointerPosition();
}
