package com.wallacehub.whholocolorpicker.whholocolorpicker;

import android.content.Context;
import android.graphics.Color;
import android.graphics.LinearGradient;
import android.graphics.Shader;
import android.util.AttributeSet;
import com.wallacehub.utils.logging.WHLog;

public class AlphaBar extends Bar_Base
{
	//<editor-fold desc="TAG">
	@SuppressWarnings({"FieldNameHidesFieldInSuperclass", "unused"})
	public static final String TAG = AlphaBar.class.getSimpleName();

	static {
		WHLog.LOG_LEVELS.put(TAG, WHLog.WARN);
	}
	//</editor-fold>

	//<editor-fold desc="Variables">
	/**
	 * Factor used to calculate the position of the pointer on the bar.
	 */
	private float m_posToValueFactor;

	/**
	 * Factor used to calculate the value vis-a-vis the pointer on the bar.
	 */
	private float m_valueToPosFactor;
	//</editor-fold>


	//<editor-fold desc="Constructors">
	public AlphaBar(Context context) {
		this(context, null);
	}


	public AlphaBar(Context context, AttributeSet attrs) {
		this(context, attrs, 0);
	}


	public AlphaBar(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
	}


	@SuppressWarnings("UnusedParameters")
	protected void init(final Context context, final AttributeSet attrs, final int defStyle) {
		super.init(context, attrs, defStyle);

		m_alpha = 0xFF;

		m_posToValueFactor = 0xFF / m_barLength;
		m_valueToPosFactor = m_barLength / 0xFF;
	}
	//</editor-fold>


	/**
	 * Get the currently selected opacity.
	 *
	 * @return The int m_alpha of the currently selected opacity.
	 */
	public int getOpacity() {
		return m_alpha;
	}


	@Override
	public void updateColor(final int alpha, final float[] hsvColor) {
		super.updateColor(alpha, hsvColor);

		m_pointerPaint.setColor(Color.HSVToColor(m_alpha, m_color));

		updateShader();

		updatePointerPosition();

		invalidate();
	}


	@Override
	protected void updateShader() {
		WHLog.d(TAG, "updateShader");

		final float width = m_orientationHorizontal == ORIENTATION_HORIZONTAL ? m_barLength : m_barThickness;
		final float height = m_orientationHorizontal == ORIENTATION_HORIZONTAL ? m_barThickness : m_barLength;

		final int colorTransparent = Color.HSVToColor(0x00, m_color);
		final int colorOpaque = Color.HSVToColor(0xFF, m_color);

		if (!m_directionReversed) {
			m_barShader = new LinearGradient(0, 0, width, height, new int[]{colorTransparent, colorOpaque}, null, Shader.TileMode.CLAMP);
		}
		else {
			m_barShader = new LinearGradient(0, 0, width, height, new int[]{colorOpaque, colorTransparent}, null, Shader.TileMode.CLAMP);
		}

		mBarPaint.setShader(m_barShader);
	}


	@Override
	protected void updatePosToValFactor() {
		m_posToValueFactor = 0xFF / m_barLength;
	}


	@Override
	protected void updateValToPosFactor() {
		m_valueToPosFactor = m_barLength / 0xFF;
	}


	@Override
	protected void updatePointerPosition() {
		float newPosition = m_valueToPosFactor * m_alpha;

		if (m_orientationHorizontal) {
			newPosition = m_directionReversed ? mBarRect.right - newPosition : mBarRect.left + newPosition;
			m_pointerPosition = clampf(newPosition, mBarRect.left, mBarRect.right);
		}
		else {
			newPosition = m_directionReversed ? mBarRect.bottom - newPosition : mBarRect.top + newPosition;
			m_pointerPosition = clampf(newPosition, mBarRect.top, mBarRect.bottom);
		}
	}


	@Override
	protected void updatePointerPosition(final float _x, final float _y) {

		if (m_orientationHorizontal) {
			m_pointerPosition = clampf(_x, mBarRect.left, mBarRect.right);
		}
		else {
			m_pointerPosition = clampf(_y, mBarRect.top, mBarRect.bottom);
		}
	}


	/**
	 * Calculate the color selected by the pointer on the bar.
	 */
	@Override
	protected void updatePointerColor() {
		WHLog.d(TAG, "updatePointerColor");

		final float coordinate = m_orientationHorizontal ? m_pointerPosition - mBarRect.left : m_pointerPosition - mBarRect.top;

		if (!m_directionReversed) {
			m_alpha = clamp(Math.round(m_posToValueFactor * coordinate), 0, 255);
		}
		else {
			m_alpha = clamp(255 - Math.round(m_posToValueFactor * coordinate), 0, 255);
		}

		m_pointerPaint.setColor(Color.HSVToColor(m_alpha, m_color));
	}
}