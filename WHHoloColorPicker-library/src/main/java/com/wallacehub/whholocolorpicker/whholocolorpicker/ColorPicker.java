package com.wallacehub.whholocolorpicker.whholocolorpicker;

import android.content.Context;
import android.content.res.Resources;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.RectF;
import android.os.Bundle;
import android.os.Parcelable;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;
import androidx.annotation.NonNull;
import com.wallacehub.whholocolorpicker.R;
import com.wallacehub.utils.logging.WHLog;

import java.util.ArrayList;
import java.util.List;

import static android.view.HapticFeedbackConstants.KEYBOARD_TAP;

/**
 * Displays a holo-themed color picker.
 * <p>
 * <p>
 * Use {@link #getNewColor()} to retrieve the selected color. <br>
 * Use {@link #addBar(Bar_Base)} to add a Bar. <br>
 * </p>
 */
public class ColorPicker extends View implements Bar_Base.OnColorChangedListener
{
	//<editor-fold desc="TAG" defaultstate="collapsed">
	@SuppressWarnings({"FieldNameHidesFieldInSuperclass", "unused"})
	public static final String TAG = ColorPicker.class.getSimpleName();

	static {
		WHLog.LOG_LEVELS.put(TAG, WHLog.WARN);
	}
	//</editor-fold>


	//<editor-fold defaultstate="collapsed" desc="Interfaces">


	/**
	 * An interface that is called whenever the color is changed. Currently it
	 * is always called when the color is changes.
	 * <p>
	 * This is meant to be used by the Activity, not the bars.
	 */
	public interface OnColorChangedListener
	{
		void onColorChanged(final int alpha, float[] HSV, int newColor);
	}
	//</editor-fold>

	//<editor-fold defaultstate="collapsed" desc="Variables">
	/*
	 * Constants used to save/restore the instance state.
	 */
	private static final String STATE_PARENT         = "STATE_PARENT";
	private static final String STATE_OLD_COLOR      = "STATE_OLD_COLOR";
	private static final String STATE_NEW_COLOR      = "STATE_NEW_COLOR";
	private static final String STATE_SHOW_OLD_COLOR = "STATE_SHOW_OLD_COLOR";

	private List<Bar_Base> m_bars = new ArrayList<>();

	/**
	 * The radius of the circle that shows the old and new colors. This INCLUDES the optional halo
	 */
	private float m_radius;

	/**
	 * The width of the halo of the circle.
	 */
	private int m_haloWidth;

	/**
	 * The rectangle enclosing the center inside the color wheel.
	 */
	private RectF mCenterRectangle = new RectF();

	/**
	 * Whether to show the old color in the center or not.
	 */
	private boolean m_showOldColor;

	/**
	 * The ARGB m_alpha of the center with the old selected color.
	 */
	private float[] m_oldColor;
	private int     m_oldAlpha;

	/**
	 * The ARGB m_alpha of the center with the new selected color.
	 */
	private float[] m_newColor;
	private int     m_newAlpha;

	/**
	 * Number of pixels the origin of this view is moved in X- and Y-direction.
	 * <p>
	 * <p>
	 * We use the center of this (quadratic) View as origin of our internal
	 * coordinate system. Android uses the upper left corner as origin for the
	 * View-specific coordinate system. So this is the value we use to translate
	 * from one coordinate system to the other.
	 * </p>
	 * Note: (Re)calculated in {@link #onMeasure(int, int)}.
	 * </p>
	 *
	 * @see #onDraw(android.graphics.Canvas)
	 */
	private float m_translationOffsetX;
	private float m_translationOffsetY;

	/**
	 * {@code Paint} instance used to draw the center with the old selected
	 * color.
	 */
	private Paint m_oldColorPaint;

	/**
	 * {@code Paint} instance used to draw the center with the new selected
	 * color.
	 */
	private Paint m_newColorPaint;

	/**
	 * {@code Paint} instance used to draw the halo of the center selected colors.
	 */
	private Paint m_haloPaint;

	/**
	 * This is used to get the color that is currently represented by the picker.
	 * Typically this would be the Activity.
	 * <p>
	 * {@code onColorChangedListener} instance of the onColorChangedListener
	 */
	private OnColorChangedListener m_onColorChangedListener;

	/**
	 * Used to track when the user has clicked inside the colorpicker
	 */
	private boolean m_clickedInCircle;


	//</editor-fold>


	//<editor-fold defaultstate="collapsed" desc="Constructors">
	public ColorPicker(Context context) {
		this(context, null);
	}


	public ColorPicker(Context context, AttributeSet attrs) {
		this(context, attrs, 0);
	}


	public ColorPicker(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);

		init(context, attrs, defStyle);
	}


	private void init(final Context context, AttributeSet attrs, int defStyle) {
		final TypedArray typedArray = context.obtainStyledAttributes(attrs, R.styleable.ColorPicker, defStyle, 0);
		final Resources resources = context.getResources();

		m_haloWidth = typedArray.getDimensionPixelSize(R.styleable.ColorPicker_halo_width, resources.getDimensionPixelSize(R.dimen.center_halo_width));

		typedArray.recycle();

		m_oldColor = new float[]{200, 0.5f, 0.5f};
		m_oldAlpha = 255;

		m_newColor = new float[]{100, 0.5f, 0.5f};
		m_newAlpha = 255;

		m_newColorPaint = new Paint(Paint.ANTI_ALIAS_FLAG);
		m_newColorPaint.setColor(Color.HSVToColor(m_newAlpha, m_newColor));
		m_newColorPaint.setStyle(Paint.Style.FILL);

		m_oldColorPaint = new Paint(Paint.ANTI_ALIAS_FLAG);
		m_oldColorPaint.setColor(Color.HSVToColor(m_oldAlpha, m_oldColor));
		m_oldColorPaint.setStyle(Paint.Style.FILL);

		m_haloPaint = new Paint(Paint.ANTI_ALIAS_FLAG);
		m_haloPaint.setColor(Color.BLACK);
		m_haloPaint.setAlpha(0x00);
		m_haloPaint.setStyle(Paint.Style.STROKE);
		m_haloPaint.setStrokeWidth(m_haloWidth);

		m_showOldColor = true;
	}
	//</editor-fold>


	//<editor-fold desc="Getters Setters">
	public void addBar(@NonNull final Bar_Base bar) {
		m_bars.add(bar);
		bar.setOnColorChangedListener(this);
	}


	/**
	 * Set a onColorChangedListener.
	 * Typically this would be called by the Activity
	 *
	 * @param listener {@code OnColorChangedListener}
	 */
	public void setOnColorChangedListener(final OnColorChangedListener listener) {
		m_onColorChangedListener = listener;
	}


	/**
	 * This is called by the Activity to set the initial color
	 *
	 * @param newColor int of the color.
	 */
	public void setNewColor(final int newColor) {

		final int newAlpha = Color.alpha(newColor);

		final float[] newColorHsv = new float[3];
		Color.colorToHSV(newColor, newColorHsv);

		setColor(newAlpha, newColorHsv);
	}


	/**
	 * Get the new color.
	 *
	 * @return The ARGB m_alpha of the new color
	 */
	public int getNewColor() {
		return Color.HSVToColor(m_newAlpha, m_newColor);
	}


	/**
	 * Get the new color.
	 *
	 * @return The ARGB m_alpha of the new color
	 */
	public float[] getNewColorHSV() {
		return m_newColor;
	}


	/**
	 * Get the new color.
	 *
	 * @return The ARGB m_alpha of the new color
	 */
	public int getNewAlpha() {
		return m_newAlpha;
	}


	/**
	 * This is called by the Activity to set the initial color
	 * Sets the old (previous) color. This is displayed on the left side of the inner circle
	 *
	 * @param oldColor int of the color.
	 */
	public void setOldColor(final int oldColor) {

		m_oldAlpha = Color.alpha(oldColor);
		Color.colorToHSV(oldColor, m_oldColor);

		m_oldColorPaint.setColor(Color.HSVToColor(m_oldAlpha, m_oldColor));

		invalidate();
	}


	public int getOldColor() {
		return Color.HSVToColor(m_oldAlpha, m_oldColor);
	}


	/**
	 * Set whether the old color is to be shown in the center or not
	 *
	 * @param show true if the old color is to be shown, false otherwise
	 */
	public void showOldColor(boolean show) {
		if (m_showOldColor != show) {
			m_showOldColor = show;

			invalidate();
		}
	}
	//</editor-fold>


	@Override
	protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
		WHLog.d(TAG, "onMeasure");
		WHLog.d(TAG, "widthMeasureSpec = [ " + widthMeasureSpec + " ] \nheightMeasureSpec = [ " + heightMeasureSpec + " ] \n");

		final int intrinsicSize = 2 * (100);

		int widthMode = MeasureSpec.getMode(widthMeasureSpec);
		int widthSize = MeasureSpec.getSize(widthMeasureSpec);
		int heightMode = MeasureSpec.getMode(heightMeasureSpec);
		int heightSize = MeasureSpec.getSize(heightMeasureSpec);

		int width = intrinsicSize;
		int height = intrinsicSize;

		if (widthMode == MeasureSpec.EXACTLY) {
			width = widthSize;
		}
		else if (widthMode == MeasureSpec.AT_MOST) {
			width = Math.min(intrinsicSize, widthSize);
		}

		if (heightMode == MeasureSpec.EXACTLY) {
			height = heightSize;
		}
		else if (heightMode == MeasureSpec.AT_MOST) {
			height = Math.min(intrinsicSize, heightSize);
		}

		final int min = Math.min(width, height);
		setMeasuredDimension(min, min);
	}


	@Override
	protected void onSizeChanged(int width, int height, int oldw, int oldh) {
		super.onSizeChanged(width, height, oldw, oldh);

		int min = Math.min(width, height);

		m_translationOffsetX = width / 2f;
		m_translationOffsetY = height / 2f;

		m_radius = min / 2f;

		mCenterRectangle.set(-m_radius + m_haloWidth, -m_radius + m_haloWidth, m_radius - m_haloWidth, m_radius - m_haloWidth);
	}


	@Override
	protected void onDraw(Canvas canvas) {
		WHLog.d(TAG, "onDraw");

		// All of our positions are using our internal coordinate system. Instead of translating
		// them we let Canvas do the work for us.
		canvas.translate(m_translationOffsetX, m_translationOffsetY);

		// Draw the halo of the center colors.
		canvas.drawCircle(0, 0, m_radius, m_haloPaint);

		if (m_showOldColor) {
			// Draw the old selected color in the center.
			canvas.drawArc(mCenterRectangle, 90, 180, true, m_oldColorPaint);

			// Draw the new selected color in the center.
			canvas.drawArc(mCenterRectangle, 270, 180, true, m_newColorPaint);
		}
		else {
			// Draw the new selected color in the center.
			canvas.drawCircle(0, 0, m_radius - m_haloWidth, m_newColorPaint);
		}
	}


	@Override
	public boolean onTouchEvent(MotionEvent event) {
		WHLog.d(TAG, "onTouchEvent");

		final float x = event.getX() - m_translationOffsetX;
		final float y = event.getY() - m_translationOffsetY;
		WHLog.d(TAG, "x = [ " + x + " ] \ny = [ " + y + " ] \n");

		switch (event.getAction()) {
			case MotionEvent.ACTION_DOWN: {
				WHLog.v(TAG, "ACTION_DOWN");

				m_clickedInCircle = clickedInCircle(x, y);
				if (m_clickedInCircle) {
					performHapticFeedback(KEYBOARD_TAP);

					m_haloPaint.setAlpha(0x50);

					if (m_showOldColor && x < mCenterRectangle.width() / 2) {
						setColor(m_oldAlpha, m_oldColor);
					}

					invalidate();
					return true;
				}
			}
			break;

			case MotionEvent.ACTION_UP: {
				WHLog.v(TAG, "ACTION_UP");

				if (m_clickedInCircle) {
					m_haloPaint.setAlpha(0x00);

					invalidate();

					return true;
				}
			}
			break;
		}

		return false;
	}


	private boolean clickedInCircle(final float x, final float y) {
		return x * x + y * y <= m_radius * m_radius;
	}


	/**
	 * This is called by external forces to set a new color in the wheel.
	 * Typically the external force would be a Bar_Base or the pointer
	 */
	@Override
	public void setColor(int alpha, float[] hsvColor) {
		WHLog.d(TAG, "onColorChanged");
		WHLog.d(TAG, "newColor = [ " + String.format("#%08X", (Color.HSVToColor(alpha, hsvColor))) + " ] \n");

		if (alpha == m_newAlpha
				&& hsvColor[0] == m_newColor[0]
				&& hsvColor[1] == m_newColor[1]
				&& hsvColor[2] == m_newColor[2]) {
			return;
		}

		// Set the colors internally
		m_newAlpha = alpha;
		m_newColor[0] = hsvColor[0];
		m_newColor[1] = hsvColor[1];
		m_newColor[2] = hsvColor[2];

		m_newColorPaint.setColor(Color.HSVToColor(m_newAlpha, m_newColor));
		invalidate();

		// Tell the bars to update
		for (Bar_Base bar : m_bars) {
			bar.updateColor(m_newAlpha, m_newColor);
		}

		// update any listeners (activity)
		if (null != m_onColorChangedListener) {
			m_onColorChangedListener.onColorChanged(m_newAlpha, m_newColor, Color.HSVToColor(m_newAlpha, m_newColor));
		}
	}


	@Override
	protected Parcelable onSaveInstanceState() {
		Parcelable superState = super.onSaveInstanceState();

		Bundle state = new Bundle();
		state.putParcelable(STATE_PARENT, superState);
		state.putInt(STATE_NEW_COLOR, Color.HSVToColor(m_newAlpha, m_newColor));
		state.putInt(STATE_OLD_COLOR, Color.HSVToColor(m_oldAlpha, m_oldColor));
		state.putBoolean(STATE_SHOW_OLD_COLOR, m_showOldColor);

		return state;
	}


	@Override
	protected void onRestoreInstanceState(Parcelable state) {
		Bundle savedState = (Bundle) state;

		Parcelable superState = savedState.getParcelable(STATE_PARENT);
		super.onRestoreInstanceState(superState);

		setNewColor(savedState.getInt(STATE_NEW_COLOR));
		setOldColor(savedState.getInt(STATE_OLD_COLOR));
		m_showOldColor = savedState.getBoolean(STATE_SHOW_OLD_COLOR);
	}
}