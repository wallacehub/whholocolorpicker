package com.wallacehub.whholocolorpicker.whholocolorpicker;

import android.content.Context;
import android.graphics.Color;
import android.graphics.LinearGradient;
import android.graphics.Shader;
import android.util.AttributeSet;
import com.wallacehub.utils.logging.WHLog;

public class BlueBar extends Bar_Base
{
	//<editor-fold desc="TAG">
	@SuppressWarnings({"FieldNameHidesFieldInSuperclass", "unused"})
	public static final String TAG = BlueBar.class.getSimpleName();

	static {
		WHLog.LOG_LEVELS.put(TAG, WHLog.WARN);
	}
	//</editor-fold>

	//<editor-fold desc="Variables">
	private int m_blue = 127;  // value of blue, between 0 and 255

	/**
	 * Factor used to calculate the position of the pointer on the bar.
	 */
	private float m_posToValueFactor;

	/**
	 * Factor used to calculate the value vis-a-vis the pointer on the bar.
	 */
	private float m_valueToPosFactor;
	//</editor-fold>


	//<editor-fold desc="Constructors">
	public BlueBar(Context context) {
		this(context, null);
	}


	public BlueBar(Context context, AttributeSet attrs) {
		this(context, attrs, 0);
	}


	public BlueBar(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
	}


	@SuppressWarnings("UnusedParameters")
	protected void init(final Context context, final AttributeSet attrs, final int defStyle) {
		super.init(context, attrs, defStyle);

		m_pointerPosition = m_barLength + m_pointerHaloWidth;

		m_posToValueFactor = 255 / m_barLength;
		m_valueToPosFactor = m_barLength / 255;
	}
	//</editor-fold>


	@Override
	public void updateColor(int alpha, final float[] hsvColor) {
		super.updateColor(alpha, hsvColor);

		m_blue = Color.blue(Color.HSVToColor(hsvColor));

		m_pointerPaint.setColor(Color.rgb(0, 0, m_blue));

		updateShader();

		updatePointerPosition();

		invalidate();
	}


	@Override
	protected void updateShader() {
		WHLog.d(TAG, "updateShader");

		final float width = m_orientationHorizontal == ORIENTATION_HORIZONTAL ? m_barLength : m_barThickness;
		final float height = m_orientationHorizontal == ORIENTATION_HORIZONTAL ? m_barThickness : m_barLength;

		m_barShader = new LinearGradient(0, 0, width, height, new int[]{Color.BLACK, Color.BLUE}, null, Shader.TileMode.CLAMP);

		mBarPaint.setShader(m_barShader);
	}


	@Override
	protected void updatePosToValFactor() {
		m_posToValueFactor = 255 / m_barLength;
	}


	@Override
	protected void updateValToPosFactor() {
		m_valueToPosFactor = m_barLength / 255;
	}


	@Override
	protected void updatePointerPosition() {
		if (m_orientationHorizontal) {
			float newPosition = m_blue * m_valueToPosFactor + mBarRect.left;
			m_pointerPosition = clampf(newPosition, mBarRect.left, mBarRect.right);
		}
		else {
			float newPosition = m_blue * m_valueToPosFactor + mBarRect.top;
			m_pointerPosition = clampf(newPosition, mBarRect.top, mBarRect.bottom);
		}
	}


	@Override
	protected void updatePointerPosition(final float _x, final float _y) {
		float newPosition = m_orientationHorizontal == ORIENTATION_HORIZONTAL ? _x : _y;

		if (m_orientationHorizontal) {
			m_pointerPosition = clampf(newPosition, mBarRect.left, mBarRect.right);
		}
		else {
			m_pointerPosition = clampf(newPosition, mBarRect.top, mBarRect.bottom);
		}
	}


	/**
	 * Calculate the color selected by the pointer on the bar.
	 */
	@Override
	protected void updatePointerColor() {
		WHLog.d(TAG, "updatePointerColor");

		final float coordinate = m_pointerPosition - mBarRect.left;

		final int currentColor = Color.HSVToColor(m_color);

		m_blue = (int) clampf(m_posToValueFactor * coordinate, 0, 255);

		int newColor = Color.rgb(Color.red(currentColor), Color.green(currentColor), m_blue);

		Color.colorToHSV(newColor, m_color);

		m_pointerPaint.setColor(newColor);
	}
}