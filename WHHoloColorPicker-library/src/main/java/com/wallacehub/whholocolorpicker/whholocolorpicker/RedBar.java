package com.wallacehub.whholocolorpicker.whholocolorpicker;

import android.content.Context;
import android.graphics.Color;
import android.graphics.LinearGradient;
import android.graphics.Shader;
import android.util.AttributeSet;
import com.wallacehub.utils.logging.WHLog;

public class RedBar extends Bar_Base
{
	//<editor-fold desc="TAG">
	@SuppressWarnings({"FieldNameHidesFieldInSuperclass", "unused"})
	public static final String TAG = RedBar.class.getSimpleName();

	static {
		WHLog.LOG_LEVELS.put(TAG, WHLog.WARN);
	}
	//</editor-fold>


	//<editor-fold desc="Variables" defaultstate="collapsed">
	private int m_red = 127;  // value of red, between 0 and 255

	/**
	 * Factor used to calculate the position of the pointer on the bar.
	 */
	private float m_posToValueFactor;

	/**
	 * Factor used to calculate the value vis-a-vis the pointer on the bar.
	 */
	private float m_valueToPosFactor;
	//</editor-fold>


	//<editor-fold desc="Constructors">
	public RedBar(Context context) {
		this(context, null);
	}


	public RedBar(Context context, AttributeSet attrs) {
		this(context, attrs, 0);
	}


	public RedBar(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
	}


	@SuppressWarnings("UnusedParameters")
	protected void init(final Context context, final AttributeSet attrs, final int defStyle) {
		super.init(context, attrs, defStyle);

		m_pointerPosition = m_barLength + m_pointerHaloWidth;

		m_posToValueFactor = 1 / m_barLength;
		m_valueToPosFactor = m_barLength / 1;
	}
	//</editor-fold>


	public int getRed() {
		return m_red;
	}


	@Override
	public void updateColor(int alpha, final float[] hsvColor) {
		super.updateColor(alpha, hsvColor);

		m_red = Color.red(Color.HSVToColor(hsvColor));

		m_pointerPaint.setColor(Color.rgb(m_red, 0, 0));

		updateShader();

		updatePointerPosition();

		invalidate();
	}


	@Override
	protected void updateShader() {
		WHLog.d(TAG, "updateShader");

		final float width = m_orientationHorizontal == ORIENTATION_HORIZONTAL ? m_barLength : m_barThickness;
		final float height = m_orientationHorizontal == ORIENTATION_HORIZONTAL ? m_barThickness : m_barLength;

		m_barShader = new LinearGradient(0, 0, width, height, new int[]{Color.BLACK, Color.RED}, null, Shader.TileMode.CLAMP);

		mBarPaint.setShader(m_barShader);
	}


	@Override
	protected void updatePosToValFactor() {
		m_posToValueFactor = 255 / m_barLength;
	}


	@Override
	protected void updateValToPosFactor() {
		m_valueToPosFactor = m_barLength / 255;
	}


	@Override
	protected void updatePointerPosition() {
		if (m_orientationHorizontal) {
			final float newPosition = m_red * m_valueToPosFactor + mBarRect.left;
			m_pointerPosition = clampf(newPosition, mBarRect.left, mBarRect.right);
		}
		else {
			final float newPosition = m_red * m_valueToPosFactor + mBarRect.top;
			m_pointerPosition = clampf(newPosition, mBarRect.top, mBarRect.bottom);
		}
	}


	@Override
	protected void updatePointerPosition(final float _x, final float _y) {
		final float newPosition = m_orientationHorizontal == ORIENTATION_HORIZONTAL ? _x : _y;

		if (m_orientationHorizontal) {
			m_pointerPosition = clampf(newPosition, mBarRect.left, mBarRect.right);
		}
		else {
			m_pointerPosition = clampf(newPosition, mBarRect.top, mBarRect.bottom);
		}
	}


	/**
	 * Calculate the color selected by the pointer on the bar.
	 */
	@Override
	protected void updatePointerColor() {
		WHLog.d(TAG, "updatePointerColor");

		final float coordinate = m_pointerPosition - mBarRect.left;

		final int currentColor = Color.HSVToColor(m_color);

		m_red = (int) clampf(m_posToValueFactor * coordinate, 0, 255);

		int newColor = Color.rgb(m_red, Color.green(currentColor), Color.blue(currentColor));

		Color.colorToHSV(newColor, m_color);

		m_pointerPaint.setColor(newColor);
	}
}