package com.wallacehub.whholocolorpicker.whholocolorpicker;

import android.content.Context;
import android.graphics.Color;
import android.util.AttributeSet;
import com.wallacehub.utils.logging.WHLog;

public class TemperatureBar extends Bar_Base
{
	//<editor-fold desc="TAG">
	@SuppressWarnings({"FieldNameHidesFieldInSuperclass", "unused"})
	public static final String TAG = TemperatureBar.class.getSimpleName();

	static {
		WHLog.LOG_LEVELS.put(TAG, WHLog.WARN);
	}
	//</editor-fold>

	/**
	 * Factor used to calculate the position of the pointer on the bar.
	 */
	private float m_posToValueFactor;

	/**
	 * Factor used to calculate the value vis-a-vis the pointer on the bar.
	 */
	private float m_valueToPosFactor;


	//<editor-fold desc="Constructors">
	public TemperatureBar(Context context) {
		super(context);
		init(null, 0);
	}


	public TemperatureBar(Context context, AttributeSet attrs) {
		super(context, attrs);
		init(attrs, 0);
	}


	public TemperatureBar(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
		init(attrs, defStyle);
	}


	@SuppressWarnings("UnusedParameters")
	protected void init(AttributeSet attrs, int defStyle) {
		m_pointerPosition = m_barLength + m_pointerHaloWidth;

		m_posToValueFactor = 9000 / ((float) m_barLength);
		m_valueToPosFactor = ((float) m_barLength) / 9000;
	}
	//</editor-fold>


	@Override
	public void updateColor(final int alpha, final float[] hsvColor) {
		super.updateColor(alpha, hsvColor);

	}


	@Override
	protected void updateShader() {
		float width, height;
		if (m_orientationHorizontal == ORIENTATION_HORIZONTAL) {
			width = (m_barLength + m_pointerHaloWidth);
			height = m_barThickness;
		}
		else {
			width = m_barThickness;
			height = (m_barLength + m_pointerHaloWidth);
		}

//		m_barShader = new LinearGradient(m_pointerHaloWidth, 0, width, height, new int[]{temperatureToColor(3500), temperatureToColor(9000)}, null, Shader.TileMode.CLAMP);
		mBarPaint.setShader(m_barShader);
	}


	@Override
	protected void updatePosToValFactor() {
		m_posToValueFactor = 9000 / ((float) m_barLength);
	}


	@Override
	protected void updateValToPosFactor() {
		m_valueToPosFactor = ((float) m_barLength) / 9000;
	}


	@Override
	protected void updatePointerPosition() {
		m_pointerPosition = Math.round((m_valueToPosFactor * m_color[1]) + m_pointerHaloWidth);
	}


	@Override
	protected void updatePointerPosition(final float _x, final float _y) {
		m_pointerPosition = Math.round((m_valueToPosFactor * m_color[1]) + m_pointerHaloWidth);
	}


	/**
	 * Calculate the color selected by the pointer on the bar.
	 */
	@Override
	protected void updatePointerColor() {
		float coordinate = m_pointerPosition - m_pointerHaloWidth;

		if (coordinate < 0) {
			coordinate = 0;
		}
		else if (coordinate > m_barLength) {
			coordinate = m_barLength;
		}

		temperatureToColor((int) (m_posToValueFactor * coordinate));
	}


	/**
	 * Calculate the color based on the selected temperature in Kelvin.
	 *
	 * @param temperature Temperature in Kelvin.
	 */
	private float[] temperatureToColor(int temperature) {
		int temp = temperature / 100;
		int tRed;
		int tGrn;
		int tBlu;

		if (temp <= 66) {
			tRed = 255;
			tGrn = temp;
			tGrn = ((int) Math.round(99.4708025861 * Math.log(tGrn) - 161.1195681661));

			if (temp <= 19) {
				tBlu = 0;
			}
			else {
				tBlu = temp - 10;
				tBlu = ((int) Math.round(138.5177312231 * Math.log(tBlu) - 305.0447927307));
			}
		}
		else {
			tRed = temp - 60;
			tRed = ((int) Math.round(329.698727446 * Math.pow(tRed, -0.1332047592)));

			tGrn = temp - 60;
			tGrn = ((int) Math.round(288.1221695283 * Math.pow(tGrn, -0.0755148492)));

			tBlu = 255;
		}

		int r = clamp(tRed, 0, 255);
		int g = clamp(tGrn, 0, 255);
		int b = clamp(tBlu, 0, 255);
		float[] tHSV = new float[3];
		Color.RGBToHSV(r, g, b, tHSV);
		return tHSV;
	}
}