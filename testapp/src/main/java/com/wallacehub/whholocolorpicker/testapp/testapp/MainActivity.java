package com.wallacehub.whholocolorpicker.testapp.testapp;

import android.graphics.Color;
import android.os.Bundle;
import androidx.appcompat.app.AppCompatActivity;
import com.wallacehub.whholocolorpicker.testapp.R;
import com.wallacehub.whholocolorpicker.whholocolorpicker.Bar_Base;
import com.wallacehub.whholocolorpicker.whholocolorpicker.ColorPicker;

public class MainActivity extends AppCompatActivity implements ColorPicker.OnColorChangedListener
{

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);

		ColorPicker picker = (ColorPicker) findViewById(R.id.picker);

		picker.addBar((Bar_Base) findViewById(R.id.svbar));
		picker.addBar((Bar_Base) findViewById(R.id.opacitybar));
		picker.addBar((Bar_Base) findViewById(R.id.saturationbar));
		picker.addBar((Bar_Base) findViewById(R.id.valuebar));
		picker.addBar((Bar_Base) findViewById(R.id.red_bar));
		picker.addBar((Bar_Base) findViewById(R.id.green_bar));
		picker.addBar((Bar_Base) findViewById(R.id.blue_bar));
		picker.addBar((Bar_Base) findViewById(R.id.hue_wheel));

		picker.addBar((Bar_Base) findViewById(R.id.alphabar_vert));
		picker.addBar((Bar_Base) findViewById(R.id.svbar_vert));
		picker.addBar((Bar_Base) findViewById(R.id.saturationbar_vert));
		picker.addBar((Bar_Base) findViewById(R.id.valuebar_vert));
		picker.addBar((Bar_Base) findViewById(R.id.redbar_vert));
		picker.addBar((Bar_Base) findViewById(R.id.greenbar_vert));
		picker.addBar((Bar_Base) findViewById(R.id.bluebar_vert));

		picker.addBar((Bar_Base) findViewById(R.id.alphabar_vert_rev));
		picker.addBar((Bar_Base) findViewById(R.id.alphabar_rev));

		//To get the color
		picker.getNewColor();

		picker.setNewColor(Color.BLUE);

		//To set the old selected color u can do it like this
		picker.setOldColor(Color.GREEN);

		// adds listener to the colorpicker which is implemented in the activity
		picker.setOnColorChangedListener(this);

		//to turn of showing the old color
		picker.showOldColor(true);
	}


	@Override
	public void onColorChanged(final int alpha, final float[] hsvColor, final int color) {

	}
}
